#include <stdio.h>
#include "subjc.h"
#include <assert.h>

/* Possible TODO to fix crashes:
 * Hooking objc before main() may be naughty. Perhaps
 * I could hook main() and run init() there?
 */

FILE *logfh;

 __attribute__((constructor))
static void init(void) {
	// TODO make this an env var
	logfh = fopen("log.txt", "w");
	assert(logfh);
	SubjC_initialize();
    SubjC_set_file(logfh);
    SubjC_start();
}

 __attribute__((destructor))
static void fini(void) {
	SubjC_end();
	fclose(logfh);
}
