#CXX=clang++
CC=clang
CXX=$(HOME)/code/llvm/tot/prefix-ninja/bin/clang++ -I /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/../lib/c++/v1
#CC=$(HOME)/code/llvm/tot/prefix-ninja/bin/clang 

CFLAGS:=-Wall -O0 -g $(CFLAGS) -stdlib=libc++ -DUSE_TLS
CXXFLAGS=-std=c++11

SUB=$(SUBSTRATE)
B32 = -arch i386
B64 = -arch x86_64


OPTIONS= \
	-I/opt/local/include \
	-I$(SUB)

%_32.o::	%.c
	$(CC) $(CFLAGS) $(OPTIONS) -c -o $@ $^ $(B32)

%_64.o::	%.c
	$(CC) $(CFLAGS) $(OPTIONS) -c -o $@ $^ $(B64)

%_32.o::	%.m
	$(CC) $(CFLAGS) $(OPTIONS) -c -o $@ $^ $(B32)

%_64.o::	%.m
	$(CC) $(CFLAGS) $(OPTIONS) -c -o $@ $^ $(B64)

%_32.o::	%.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(OPTIONS) -c -o $@ $^ $(B32)

%_64.o::	%.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(OPTIONS) -c -o $@ $^ $(B64)

%_32.o::	%.mm
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(OPTIONS) -c -o $@ $^ $(B32)

%_64.o::	%.mm
	$(CXX) $(CFLAGS) $(CXXFLAGS) $(OPTIONS) -c -o $@ $^ $(B64)

C_TUS = balanced_substr crc32
CXX_TUS = subjc objc_type string_util pseudo_base64
TUS=$(C_TUS) $(CXX_TUS)


all: libsubjc.dylib libprobe.dylib

libsubjc_32.dylib: $(addsuffix _32.o,$(TUS))
	$(CXX) $(CFLAGS) -framework Foundation -dynamiclib -o $@ $^  -L$(SUB) -lsubstrate $(B32)

libsubjc_64.dylib: $(addsuffix _64.o,$(TUS))
	$(CXX) $(CFLAGS) -framework Foundation -dynamiclib -o $@ $^  -L$(SUB) -lsubstrate $(B64)

libsubjc.dylib: libsubjc_32.dylib libsubjc_64.dylib
	lipo -create $^ -output $@

libprobe_32.dylib: probe_32.o libsubjc.dylib
	$(CC) $(CFLAGS) -dynamiclib -o $@ $^ -L$(SUB) -lsubstrate -L. -lsubjc $(B32)

libprobe_64.dylib: probe_64.o libsubjc.dylib
	$(CC) $(CFLAGS) -dynamiclib -o $@ $^ -L$(SUB) -lsubstrate -L. -lsubjc $(B64)

libprobe.dylib: libprobe_32.dylib libprobe_64.dylib
	lipo -create $^ -output $@

clean:
	rm -f *.o *.dylib
	rm -rf *.dSYM

.PHONY:	all clean
